//Axel Brochu 2233104
package application;

import vehicles.Bicycle;

public class BikeStore {
    public static void main(String[] args){
        Bicycle[] bikes = new Bicycle[4];

        bikes[0] = new Bicycle("Toyota", 12, 25);
        bikes[1] = new Bicycle("KFC", 4, 15);
        bikes[2] = new Bicycle("Bethesda", 6, 17);
        bikes[3] = new Bicycle("Oracle", 9, 29);
        
        for(Bicycle bike : bikes)
            System.out.println(bike);
    }
}
